Contents 
==========
[**Practical 1**](#practical-1)  
[**Practical 2**](#practical-2)  
[**Practical 3**](#practical-3)  
[**Practical 4**](#practical-4)  
[**Practical 5**](#practical-5)  

\newpage

# Practical 1

Work Breakdown Structure (WBS) is a tool or key project deliverable that organises a team's work into a simpler and manageable section or task.
These are simply deliverable hierarchical decomposition of work that is needed to be executed by the project team.  

> __**How to Schedule a project with a work breakdown structure:**__  
>    Add tasks until your work breakdown structure is complete.  
>    Select a task and click Indent to move it under another task or click Outdent to move it out a level.  
>    Select a task and click Move Up or Move Down to move it up or down in the list.  
>    Click Hide Gantt to hide the Gantt chart, and click Show Gantt to display it again.  
>    Select a different period of time for the Gantt chart in Time Scale.  
>    Add predecessors  
>    Click Save at the bottom right corner of the screen when you’re done making changes.  

## Steps: 
  1. Add tasks and indent required tasks:  
![](./media/1.png){ width=70% }  

  1. Add duration and auto schedule:    
![](./media/2.png){ width=70% }  

  1. Add predecessors:  
![](./media/3.png){ width=70% }  

## Gantt Chart:
![](./media/4.png){ width=70% }  

>  **Here we’ve created a main task called Project life cycle:** 
>  
>> Under this project life cycle we have 3 major subtasks namely identify, planning and execute.  
>
>> Under the identify sub-task, we have further sub-tasks which include requirement gathering and business analysis.  
>
>> Under the planning sub-task, we have further sub-tasks which include master test plan, development strategy and define performance.  
>
>>  Under execute sub-task, we have further subtasks which include deliveries and correction of errors  
>
>> Duration of each sub-sub task is defined and first, the duration of each subtask task is calculated by estimating the duration of the largest of its subtask, but, if predecessors are specified, then, the duration will be the sum of duration of all the sub-sub-tasks.  
>
>> In the images, it can be observed that for identify subtask, duration of each sub-sub-task is 3 days thus the duration of identity will be 6days  
>
>> Similarly, the duration of planning is 13 days (4+5+4 days) and that of execute sub-task is 10 days (3+7 days).  
>
>> This makes the duration of the overall project cycle to be 6 + 13 + 10 = 29 days.  
>
>> Predecessor of planning is identify and that of execute is planning.  
>
>> Predecessor of business analytics is requirement gathering (under identify)  
>
>> Predecessor of development strategy is master test plan and that of define performance is development strategy.  
>
>> Similarly for Execute, the predecessor of correction of errors is deliveries.  
>
>> Closure will be the goal whose duration has to be 0 days.  

---  

\newpage

# Practical 2

### Naming conventions in WBS:
![](./media/5.png){ width=70% }   
  
![](./media/6.png){ width=70% }  

* Naming conventions are assigned in this WBS under the column named WBS.  

  > The first layer includes naming on the basis of larger case english alphabets separated by ‘.’  
  > The second layer includes naming on the basis of small case english alphabets separated by ‘.’  
  > A.a indicates 1st sub-task(identify), A.b indicates 2nd sub-task(planning), A.c indicates 3rd sub-task(execute) and A.d indicates closure.  
  > A.a.1, A.b.3, A.c.2 indicate the sub-sub-tasks.  
  
* The black solid lines in the gantt chart indicate the length of the subtasks and blue solid lines indicate the length of sub-sub-tasks.  
* Closure is represented by a black solid diamond.  
* **Gantt chart** clearly indicates the flow of the project:  
![](./media/7.png)

---  

\newpage

# Practical 3

## **Status Bar**  
  If you right-click anywhere on the Status Bar, you’ll see a list of potential contents.  
![](./media/8.png){ width=70% }  

1. **Cell mode**: One of the items in the status bar is Ready, which is the cell mode. If you click Cell Mode, It no longer says Ready because the cell mode option is not selected.   
![](./media/9.png){ width=70% }  

2. **New tasks**: See whether new tasks are manually scheduled or auto-scheduled. If you click New Tasks it will no longer show that currently they’re manually scheduled by default.  
![](./media/10.png){ width=70% }  

3. **View Shortcuts**: This corresponds to this set of buttons on the right side of status bar that can be used to enable different views of my project:  
    * __Gantt chart view__:  
![](./media/11.png){ width=70% }  

    * __Task usage view__:  
![](./media/28.png){ width=70% }  

    * __Team planner view__:   
![](./media/12.png){ width=70% }  

    * __Resource sheet view__:  
![](./media/29.png){ width=70% }  

4. **Zoom Slider**: The very bottom option is the Zoom Slider and this gives access to the control that can be used to zoom in and out of the current view.  
![](./media/13.png){ width=70% }  

---  

\newpage

# Practical 4

## Types of Predecessors:

1. **Finish-to-Start(FS)**:
In Finish-to-start the successor (second task) cannot begin until its predecessor (first task) is complete.    
![](./media/14.png){ width=55% }  
![](./media/15.png){ width=65% }  

2. **Start-to-Start(SS)**:  
A Start-to-Start (or SS) dependency means that a successor activity cannot begin before its predecessor has started.   
![](./media/16.png){ width=55% }  
![](./media/17.png){ width=65% }  

3. **Finish-to-Finish(FF)**:  
Finish-to-finish (FF) means that the successor activity can only finish after its predecessor has been completed.  
![](./media/18.png){ width=55% }  
![](./media/19.png){ width=65% }  

4. **Start-to-Finish (SF)**:  
Start-to-finish occurs when the completion of the successor activity depends on the initiation of the predecessor activity.  
![](./media/20.png){ width=55% }  
![](./media/21.png){ width=65% }  


> Adding Lag:  
>  
  >> Lag refers to the amount of time that is added between a predecessor task and its successor. It can be applied to all dependent tasks regardless of their dependency type (FS, FF, SS, SF). Lag is always associated with delay.  
>
![](./media/22.png){ width=55% }  
![](./media/23.png){ width=65% }  
>  

---  

\newpage

# Practical 5

## Creation and Assigning of Task Calendar:  
This function assists us to define a calendar to a particular task. Because sometimes few of our tasks follow different schedules and thus a customized calendar is required for such tasks.  
**Steps**:  

  1. Create a calendar named “task calendar”.  
  ![](./media/24.png){ width=70% }  

  1. After the creation of task calendar, define the number of working days and hours.  
  ![](./media/25.png){ width=70% }  

  1. Define exceptions i.e. holidays.  
  ![](./media/26.png){ width=70% }  

  1. Assign the task calendar to the required task.  
  ![](./media/27.png){ width=70% }  

  1. Task calendar is assigned to the task named “Development strategies”.   


